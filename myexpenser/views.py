from django.shortcuts import render
from django.http import HttpResponse,HttpResponseRedirect
from models import Bill, BillCycle, ClaimCycle, Claim
import xlwt,re,sys
from datetime import timedelta,date,datetime
from django.conf import settings
from os import path,unlink,listdir
from django.contrib import messages
import zipfile
from django.core.mail import send_mail,EmailMultiAlternatives

def check_file_exists(file_path):
    try:
        if path.isfile(file_path) and path.exists(file_path) and path.getsize(file_path) > 0:
            return True
        else:
            return False
    except (IOError,OSError):
        return False


def clear_file(file_path):
    try:
        if file_path:        
            if check_file_exists(file_path):
                unlink(file_path)
    except:
        pass

def zipdir(path, ziph):
    # ziph is zipfile handle
    for root, dirs, files in os.walk(path):
        for file in files:
            ziph.write(os.path.join(root, file))

def get_report_path(name,ext):
    filepath = settings.REPORT_ROOT
    date_and_time = str(datetime.now()).replace(':','-').replace(' ','_').split('.')[0]
    project_name = re.sub(r'[^A-Za-z0-9]', "_", '%s'%name)
    reportname = "%s_%s.%s"%(project_name,date_and_time,ext)
    full_file_path = "%s%s" % (filepath, reportname)
    return (reportname,full_file_path)

def export_tracker(request,pk):
	try:
		bill = BillCycle.objects.get(id=pk)
	except:
		messages.error(request,'BillCycle objects does not exists')
		return HttpResponseRedirect('/admin/')
	bill_date = bill.cycle_date.strftime('%B %d,%Y')
	reportname,full_file_path = get_report_path(bill_date,'xls')  
	try: 
		print 'Excel Tracker Generation initiated'             
		workbook = xlwt.Workbook()
		font = xlwt.Font()
		font.bold = True     
		pattern = xlwt.Pattern()
		print 
		style = xlwt.easyxf('pattern: pattern solid, fore_colour orange;'
			'font: colour white, bold True;align: horiz center;alignment: wrap on;')
		style1 = xlwt.XFStyle()    
		sheet = workbook.add_sheet("Bills List")   
		sheet.row(0).height = 256*2

		# All the headers are registered in list
		headers = [
			'CYCLE','NAME','REASON','SUPPORTING',
		]
		for i, header in enumerate(headers):
			sheet.write(0, i, header,style)     

		# All the column widths are registered in list   
		col_widths = [6666,6666,6666,6666]
		for i, w in enumerate(col_widths):
			sheet.col(i).width = w

		alignment = xlwt.Alignment()
		alignment.vert = xlwt.Alignment.VERT_CENTER 
		alignment.wrap = 1   
		style2 = xlwt.XFStyle()
		alignment2 = xlwt.Alignment()
		alignment2.horz = xlwt.Alignment.HORZ_CENTER
		alignment2.vert = xlwt.Alignment.VERT_CENTER 
		alignment2.wrap = xlwt.Alignment.WRAP_AT_RIGHT 
		style2.alignment=alignment2
		style1.alignment=alignment 
		bills = Bill.objects.filter(cycle_id=pk)
		zip_report_name,zip_file_path = get_report_path(bill_date,'zip')
		zipf = zipfile.ZipFile(zip_file_path, 'w', zipfile.ZIP_DEFLATED)
		i = 1 
		for bill in bills:     
			sheet.write(i, 0, '%s'%bill.cycle ,style2)
			sheet.write(i, 1, '%s'%bill.name ,style2)
			sheet.write(i, 2, '%s'%bill.reason ,style2)
			sheet.write(i, 3, '%s'%bill.supporting ,style2)
			zipf.write('%s'%bill.supporting)
			i += 1
		zipf.close()

		workbook.save(full_file_path)        
		print 'Excel Tracker generated'
		# files = [
		# 		(reportname,full_file_path,"application/vnd.ms-excel"),
		# 		(zip_report_name,zip_file_path,"application/pdf")
		# ]
		# m = EmailMultiAlternatives('Expense Bills', 'PFA', 'abhay@we45.com', ['abhay@we45.com'])
		# for r,p,e in files:
		# 	m.attach(r, open(p, 'rb').read(), e)
		# m.send() 
		messages.success(request,'Reports generated and mailed')
		return HttpResponseRedirect('/admin/')		
	except BaseException as e:
		exc_type, exc_value, exc_traceback = sys.exc_info()
		print "Line no :%s Exception %s"%(exc_traceback.tb_lineno,e)
		messages.error(request,'Error')
		return HttpResponseRedirect('/admin/')


def export_claim_tracker(request,pk):
    try:
	claim = ClaimCycle.objects.get(id=pk)
    except:
	messages.error(request,'ClaimCycle objects does not exist')
	return HttpResponseRedirect('/admin/')
    bill_date = claim.name
    reportname,full_file_path = get_report_path(bill_date,'xls')  
    try: 
	print 'Excel Tracker Generation initiated'             
	workbook = xlwt.Workbook()
	font = xlwt.Font()
	font.bold = True     
	pattern = xlwt.Pattern()
	print 
	style = xlwt.easyxf('pattern: pattern solid, fore_colour orange;'
	                    'font: colour white, bold True;align: horiz center;alignment: wrap on;')
	style1 = xlwt.XFStyle()    
	sheet = workbook.add_sheet("Claims List")   
	sheet.row(0).height = 256*2

	# All the headers are registered in list
	headers = [
	    'Claim Cycle','NAME','REASON','SUPPORTING',"Amount"
	]
	for i, header in enumerate(headers):
	    sheet.write(0, i, header,style)     

	# All the column widths are registered in list   
	col_widths = [6666,6666,6666,6666,6666]
	for i, w in enumerate(col_widths):
	    sheet.col(i).width = w

	alignment = xlwt.Alignment()
	alignment.vert = xlwt.Alignment.VERT_CENTER 
	alignment.wrap = 1   
	style2 = xlwt.XFStyle()
	alignment2 = xlwt.Alignment()
	alignment2.horz = xlwt.Alignment.HORZ_CENTER
	alignment2.vert = xlwt.Alignment.VERT_CENTER 
	alignment2.wrap = xlwt.Alignment.WRAP_AT_RIGHT 
	style2.alignment=alignment2
	style1.alignment=alignment 
	claims = Claim.objects.filter(cycle_id=pk)
	zip_report_name,zip_file_path = get_report_path(bill_date,'zip')
	zipf = zipfile.ZipFile(zip_file_path, 'w', zipfile.ZIP_DEFLATED)
	i = 1 
	for bill in claims:     
	    sheet.write(i, 0, '%s'%bill.cycle ,style2)
	    sheet.write(i, 1, '%s'%bill.name ,style2)
	    sheet.write(i, 2, '%s'%bill.reason ,style2)
	    sheet.write(i, 3, '%s'%bill.supportings ,style2)
	    sheet.write(i, 4, '%d'%bill.amount)
	    zipf.write('%s'%bill.supportings)
	    i += 1
	zipf.close()

	workbook.save(full_file_path)        
	print 'Excel Tracker generated'
	# files = [
	# 		(reportname,full_file_path,"application/vnd.ms-excel"),
	# 		(zip_report_name,zip_file_path,"application/pdf")
	# ]
	# m = EmailMultiAlternatives('Expense Bills', 'PFA', 'abhay@we45.com', ['abhay@we45.com'])
	# for r,p,e in files:
	# 	m.attach(r, open(p, 'rb').read(), e)
	# m.send() 
	messages.success(request,'Reports generated and mailed')
	return HttpResponseRedirect('/admin/')		
    except BaseException as e:
	exc_type, exc_value, exc_traceback = sys.exc_info()
	print "Line no :%s Exception %s"%(exc_traceback.tb_lineno,e)
	messages.error(request,'Error')
	return HttpResponseRedirect('/admin/')


