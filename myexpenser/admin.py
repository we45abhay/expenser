from django.contrib import admin
from models import Bill, BillCycle, ClaimCycle, Claim
from django.conf import settings


class BillCycleAdmin(admin.ModelAdmin):
	list_display = ( 'cycle_date','export_report')	

	def export_report(self, obj):			
		return '<a href="/export/%s/" class="btn btn-info" style="float:right;">Export</a>' %obj.id 
	export_report.allow_tags = True
	export_report.short_description = ''
	
class ClaimCycleAdmin(admin.ModelAdmin):
	list_display = ( 'name','export_claim_report')	

	def export_claim_report(self, obj):			
		return '<a href="/export_claims/%s/" class="btn btn-info" style="float:right;">Export</a>' %obj.id 
	export_claim_report.allow_tags = True
	export_claim_report.short_description = ''

admin.site.register(BillCycle,BillCycleAdmin)
admin.site.register(Bill)

admin.site.register(ClaimCycle, ClaimCycleAdmin)
admin.site.register(Claim)