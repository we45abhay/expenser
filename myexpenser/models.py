from __future__ import unicode_literals

from django.db import models

# Create your models here.
class BillCycle(models.Model):
	cycle_date = models.DateField()

	def __unicode__(self):
		return str(self.cycle_date)

class Bill(models.Model):
	cycle = models.ForeignKey(BillCycle)
	name = models.CharField(max_length=50)
	reason = models.TextField(null=True, blank=True)
	supporting = models.FileField(upload_to = 'supportings/')
	
	def __unicode__(self):
		return str(self.name + ' - ' + str(self.cycle.cycle_date))	


class ClaimCycle(models.Model):
	name = models.CharField(max_length=20)
	
	def __unicode__(self):
		return str(self.name)
	
	

class Claim(models.Model):
	cycle = models.ForeignKey(ClaimCycle)
	name = models.CharField(max_length = 50)
	reason = models.TextField(null=True, blank=True)
	supportings = models.FileField(upload_to = 'supportings/')
	amount = models.DecimalField(max_digits = 8, decimal_places=2, null=True, blank=True)
	
	def __unicode__(self):
		return str(self.name + ' - ' + str(self.cycle.name))	
	

	

